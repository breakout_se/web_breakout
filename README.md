BREAKOUT

This is a arcade game building for Software Engineering Class.

Hardware : Raspberry Pi3 Model B
	- CPU 1.2Ghz 64bit quad-core ARM Cortex A-53
	- GPU 400MHz VideoCore IV
	- Broadcom BCM2837
	- Wireless Connectivity 802.11n Bluetooth 4.1
For difference between the Pi's check this http://wiki.seeed.cc/Raspberry_Pi_3_Model_B/

Software:
OS : Raspbia Stretch https://www.raspberrypi.org/downloads/raspbian/
Docker containers are used to host a server and a database.Dockerhub request will soon be sent out. 
How to Pull the Images:
	Two images are required to install the game.One for the server and other one for databse.Both the images can be downloaded from Docker Hub using the commands 
$ docker pull sumanthnr27/breakout_game
$ docker pull sumanthnr27/database
Now create the containers using the images which are downloaded above.
$ docker network create –d bridge <bridge-name>
$ docker run –it –net=<bridge-name> --name server –p <host_ip_address:3000> -p <host_ip_address:3001> breakout_game /bin/bash
#root@hgnh638nc:/usr/app# npm run deploy
Open another terminal and enter the following
$ docker run –net =<bridge-name> --name  db –e MYSQL_ROOT_PASSWORD=mypass database 
$ docker exec –it db /bin/bash
# (check the database table and entries)
Open the browser and enter your host ip address to start the game.
Domain Name Server: The domain name for this project is obtained from noip.com which provides free domain names. Create an account and create a host name by using your public ipv4 address, for this free version the domain name is valid for 30 days and has to be renewed every month.